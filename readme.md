В этом сниппете находятся скрипты для запуска прокси-сервера Traefik - https://docs.traefik.io/master/

Файл acme.json должен иметь права 600

Для того чтобы Traefik получил доступ до вашего контейнера необходимо в compose-файле вашего контейнера добавить сеть Traefik и метки:
```
version: '2.1'
services:
 container_name:
   ....
   labels:
    - traefik.enable=true
    - traefik.backend=container_name
    - traefik.docker.network=traefik_proxy
    - traefik.frontend.rule=Host:domain.example.com
    - traefik.port=8080
   networks:
     - traefik

networks:
 traefik:
   external:
     name: traefik_proxy

```
